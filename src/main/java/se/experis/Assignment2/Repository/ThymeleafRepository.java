package se.experis.Assignment2.Repository;

import se.experis.Assignment2.models.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ThymeleafRepository {
    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;


    /*
    Method that uses the "random()" function in sql to get 5 random tracks/songs,
    saves it in a track-list and returns it to the controller.
     */
    public ArrayList<Tracks> selectRandomSongs() {
        ArrayList<Tracks> tracks = new ArrayList<Tracks>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Track\n" +
                            "ORDER BY random()\n" +
                            "LIMIT 5");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                tracks.add(
                        new Tracks(
                                resultSet.getString(1)
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return tracks;
        }
    }

    /*
        Method that uses the "random()" function in sql to get 5 random artists,
        saves it in a artist-list and returns it to the controller.
         */
    public ArrayList<Artists> selectRandomArtists() {
        ArrayList<Artists> artists = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Track\n" +
                            "ORDER BY random()\n" +
                            "LIMIT 5");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                artists.add(
                        new Artists(
                                resultSet.getString(1)
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return artists;
        }
    }

    /*
    Method that uses the "random()" function in sql to get 5 random genres,
    saves it in a genre-list and returns it to the controller.
     */
    public ArrayList<Genre> selectRandomGenres() {
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Track\n" +
                            "ORDER BY random()\n" +
                            "LIMIT 5");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                genres.add(
                        new Genre(
                                resultSet.getString(1)
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return genres;
        }
    }

    /*
    Method that takes in a string to check if it contains in a track-name using WHERE track-name LIKE "searchTerm". If it does it selects the
    track-name, artist-name, album-title, genre-name and adds it to a trackInfo-list. This list are then returned to
    the controller.
     */
    public ArrayList<TrackInfo> searchTracksByName(String query) {
        ArrayList<TrackInfo> trackList = new ArrayList<>();

        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT T.Name, A2.Name, A.Title, G.Name\n" +
                            "FROM Track AS T\n" +
                            "INNER JOIN Genre AS G\n" +
                            "on T.GenreId = G.GenreId\n" +
                            "INNER JOIN Album A\n" +
                            "on T.AlbumId = A.AlbumId\n" +
                            "INNER JOIN Artist as A2\n" +
                            "on A.ArtistId = A2.ArtistId\n" +
                            "WHERE T.Name\n" +
                            "LIKE ?");
            // Execute Statement
            preparedStatement.setString(1, "%" + query + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                trackList.add(
                        new TrackInfo(
                                resultSet.getString(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4)
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return trackList;
        }
    }


}
