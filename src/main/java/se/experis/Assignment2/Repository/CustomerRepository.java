package se.experis.Assignment2.Repository;

import se.experis.Assignment2.models.Customer;
import se.experis.Assignment2.models.CustomerCountry;
import se.experis.Assignment2.models.CustomerSpenders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CustomerRepository {
    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;


    /*
    Method that calls to get all the customers from the database. It prepares a statement with a query
    that return all necessary information about a customer. The customers gets added to a list, the list
    is then returned to the controller. If something with the connection went wrong it throws a exception.
     */
    public ArrayList<Customer> selectAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return customers;
        }
    }

    /*
    Method that returns a specific customer by checking a customers customerId. The query checks all customers in the database and
    the WHERE statement checks the id. If a customer contains the id it returns it to the controller.
     */
    public Customer SelectCustomerById(int id) {
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer WHERE CustomerId = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return customer;
        }
    }

    /*
    Method that returns a specific customer by checking a customers name. The query checks all customers in the database and
    the LIKE statement checks if a customer has the name as FirstName. If a customer contains the name it returns it to the controller.
     */
    public Customer SelectCustomerByName(String name) {
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer WHERE FirstName LIKE ?");
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return customer;
        }
    }

    /*
    Method that uses a limit and offset to return a an amount of customer choosed in the database. The offset decide where in the list
    to start and the limit decide how many after the start it should add to the list. In the query those are set using the LIMIT and OFFSET
    statements.
     */
    public ArrayList<Customer> selectPageCustomers(int limit, int offset) {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer LIMIT ? OFFSET ?");
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return customers;
        }
    }

    /*
    This method adds a customer to the database. It sends in a customer and by using INSERT in the query. Every parameter in the customer
    gets sent to their specific table. If a customer get successfully added it prints "Add customer successfully".
     */
    public Boolean addNewCustomer(Customer customer) {
        Boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Add customer succesfully");
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return success;
    }

    /*
    This method updates a customer by sending in a new customer object and by using the customerId. By using the UPDATE statement, it sets all values
    for the updated customer.
     */

    public Boolean updateExistingCustomer(Customer customer) {
        Boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer SET FirstName = ? , LastName = ?, Country = ?, PostalCode =?, Phone = ?, Email = ? WHERE CustomerId=?");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getCustomerId());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Update customer succesfully");
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return success;
    }


    /*
    Method that uses each customers home-country to see where all the buyers come from. This is later displayed in a descending order.
    The query selects the country and the amount of people from each country. DESC changes the table to be in descending order (ORDER BY count(*))
     */
    public ArrayList<String> customersInEachCountry() {
        ArrayList<CustomerCountry> customers = new ArrayList<CustomerCountry>();
        ArrayList<String> customerInEachCountryList = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Country, COUNT(*) FROM customer GROUP BY Country ORDER BY COUNT(*) DESC");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerInEachCountryList.add(resultSet.getString(1));
                customerInEachCountryList.add(resultSet.getString(2));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return customerInEachCountryList;
        }
    }
    /*
    List that returns a list of how much each customer have in total invoice. The list is ordered by DESC to get the customer
    with the highest total invoice. The SUM sums up all the invoices for a specific customer and adds it to the list.
     */

    public ArrayList<String> listCustomersByInvoiceTotal() {
        ArrayList<CustomerSpenders> customers = new ArrayList<CustomerSpenders>();
        ArrayList<String> customersByTotalInvoice = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId, SUM([Total]) From Invoice GROUP BY CustomerId ORDER BY SUM([Total]) DESC");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customersByTotalInvoice.add(resultSet.getString(1));
                customersByTotalInvoice.add(resultSet.getString(2));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return customersByTotalInvoice;
        }
    }


    /*
    Method that gets the most popular genre for a specific customer by sending in a customerId
    Its gets all the invoices with specific row of different genres and how many of each genres a customer
    has bought. This is then ordered by how many of each genre in descending order. When adding to the list
    it checks if is a tie between multiple genres, if that is the case it adds all to the list. Otherwise only one genre is added.
     */
    public ArrayList<String> mostPopularGenreById(int customerId) {

        ArrayList<String> results = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT FirstName, count(G.GenreId),G.GenreId,G.Name\n" +
                            "FROM Customer AS C\n" +
                            "    INNER JOIN Invoice I\n" +
                            "    on C.CustomerId = I.CustomerId\n" +
                            "    INNER JOIN InvoiceLine IL\n" +
                            "    on I.InvoiceId = IL.InvoiceId\n" +
                            "    INNER JOIN Track T\n" +
                            "    on IL.TrackId = T.TrackId\n" +
                            "    INNER JOIN Genre G\n" +
                            "    on G.GenreId = T.GenreId\n" +
                            "WHERE C.CustomerId = ?\n" +
                            "GROUP BY G.GenreId\n" +
                            "ORDER BY count(G.GenreId) DESC");

            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            int temp = resultSet.getInt(2);
            while (resultSet.next()) {
                if (temp == resultSet.getInt(2)) {
                    results.add(String.valueOf(resultSet.getInt(2)));
                    results.add(String.valueOf(resultSet.getInt(3)));
                    results.add(resultSet.getString(4));
                }

            }

        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());

        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
            return results;
        }
    }


}
