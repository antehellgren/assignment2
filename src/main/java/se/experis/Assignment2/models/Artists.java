package se.experis.Assignment2.models;

public class Artists {
    String name;

    public Artists(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
