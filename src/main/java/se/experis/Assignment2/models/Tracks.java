package se.experis.Assignment2.models;

public class Tracks {
    String name;

    public Tracks(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
