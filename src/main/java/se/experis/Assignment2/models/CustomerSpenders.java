package se.experis.Assignment2.models;

public class CustomerSpenders {
    int customerId;
    String firstName;
    String lastName;

    public CustomerSpenders(int customerId, String firstName, String lastName) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
