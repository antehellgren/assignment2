package se.experis.Assignment2.controllers;

import org.springframework.web.bind.annotation.*;
import se.experis.Assignment2.models.Customer;
import se.experis.Assignment2.Repository.CustomerRepository;

import javax.sound.midi.Track;
import java.util.ArrayList;

@RestController
public class CustomerController {
    private CustomerRepository customerRepository = new CustomerRepository();

    /*
    Given the URL it gets all the customers and prints it on the browser
     */
    @RequestMapping(value = "/getAllCustomer", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomer() {
        return customerRepository.selectAllCustomers();
    }

    /*
    Given the URL it gets a specific customer by ID and prints it on the browser
    */
    @RequestMapping(value = "/SelectSpecificCustomerById/{id}", method = RequestMethod.GET)
    public Customer getSpecificCustomer(@PathVariable int id) {
        return customerRepository.SelectCustomerById(id);
    }

    /*
    Given the URL it gets a specific customer by name and prints it on the browser
    */
    @RequestMapping(value = "/SelectCustomerByName/{name}", method = RequestMethod.GET)
    public Customer getSpecificCustomerByName(@PathVariable String name) {
        return customerRepository.SelectCustomerByName(name);
    }

    /*
    Given the URL it gets a page of customers by setting a limit and offset
     */
    @RequestMapping(value = "/selectPageCustomers/{limit}/{offset}", method = RequestMethod.GET)
    public ArrayList<Customer> selectPageCustomers(@PathVariable int limit, @PathVariable int offset) {
        return customerRepository.selectPageCustomers(limit, offset);
    }

    /*
    Sends a customer using POST to the database and adds it.
     */
    @RequestMapping(value = "/addNewCustomer", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer) {
        return customerRepository.addNewCustomer(customer);
    }

    /*
    Updates a customer using PUT by its ID.
     */
    @RequestMapping(value = "/updateExistingCustomer/{id}", method = RequestMethod.PUT)
    public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer) {
        return customerRepository.updateExistingCustomer(customer);
    }

    /*
    Requests a table with all the different countries the customer live in and a count of how many in each country.
     */
    @RequestMapping(value = "/Country", method = RequestMethod.GET)
    public ArrayList<String> customersInEachCountry() {
        return customerRepository.customersInEachCountry();
    }

    /*
    Requests a list of all the customers and their total spend in descending order.
     */
    @RequestMapping(value = "/Spenders", method = RequestMethod.GET)
    public ArrayList<String> customersByInvoiceTotal() {
        return customerRepository.listCustomersByInvoiceTotal();
    }

    /*
    By a given customerId get their most popular genre.
     */
    @RequestMapping(value = "/Genre/{id}", method = RequestMethod.GET)
    public ArrayList<String> mostPopularGenre(@PathVariable int id) {
        return customerRepository.mostPopularGenreById(id);
    }


}