package se.experis.Assignment2.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import se.experis.Assignment2.Repository.ThymeleafRepository;

@Controller
public class ThymeleafController {
    ThymeleafRepository trep = new ThymeleafRepository();

    /*
    Requests 5 different tracks,artists and genres and add those attributes to a model.
    That later can handle it in a html-file.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getRandomSongs(Model model) {
        model.addAttribute("tracks", trep.selectRandomSongs());
        model.addAttribute("artists", trep.selectRandomArtists());
        model.addAttribute("genres", trep.selectRandomGenres());
        return "home";
    }

    /*
    Requests all tracks containing a specific searchterm. All the tracks are added and displayed through html.
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getTrackBySearchQuery(@RequestParam("query") String searchTerm, Model model) {
        model.addAttribute("tracks", trep.searchTracksByName(searchTerm));
        model.addAttribute("searchTerm", searchTerm);
        return "search";
    }
}
