package se.experis.Assignment2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import se.experis.Assignment2.Repository.CustomerRepository;
import se.experis.Assignment2.models.Customer;

import java.util.ArrayList;

@SpringBootApplication
public class Assignment2Application {

    public static void main(String[] args) {
        SpringApplication.run(Assignment2Application.class, args);
        CustomerRepository sqliteHelper = new CustomerRepository();

        //Customer customer = new Customer(60,"Ante","Alexander","Sweden","35256","112","A@l.se");
        //sqliteHelper.addNewCustomer(new Customer(60,"Ante","Alexander","Sweden","35256","112","A@l.se"));

		/*ArrayList<Customer> customers = sqliteHelper.selectPageCustomers(3,4);
		printCustomers(customers);*/

		/*Customer customer = sqliteHelper.SelectCustomerByName("Luis");
		printCustomer(customer);*/

        //sqliteHelper.updateExistingCustomer(new Customer(1,"Ante","Alexander","Sweden","35256","112","A@l.se"));

        //Customer customer = sqliteHelper.SelectCustomerById(1);
        //printCustomer(customer);

		/*ArrayList<Customer> customers = sqliteHelper.selectAllCustomers();
		printCustomers(customers);*/

		/*ArrayList<String> customersInEachCountry = sqliteHelper.customersInEachCountry();
		printCustomersInEach(customersInEachCountry);*/

        /*ArrayList<String> customersByTotalInvoice = sqliteHelper.listCustomersByInvoiceTotal();
		printCustomersInEach(customersByTotalInvoice);*/

        /*ArrayList<String> customersMostPopularGenre = sqliteHelper.mostPopularGenreById(45);
        printCustomersGenre(customersMostPopularGenre);*/

    }

    private static void printCustomersInEach(ArrayList<String> customersInEachCountry) {
        if (customersInEachCountry.size() != 0) {
            for (int i = 0; i < customersInEachCountry.size() - 1; i = i + 2) {
                System.out.println("-------------------------------");
                System.out.println(customersInEachCountry.get(i) + ": " + customersInEachCountry.get(i + 1));
            }
        } else {
            System.out.println("No customers returned");
        }
    }

    private static void printCustomersGenre(ArrayList<String> customersInEachCountry) {
        if (customersInEachCountry.size() != 0) {
            for (int i = 0; i < customersInEachCountry.size() - 1; i = i + 3) {
                System.out.println("-------------------------------");
                System.out.println(customersInEachCountry.get(i) + ": " + customersInEachCountry.get(i + 1)
                        + ", " + customersInEachCountry.get(i + 2));
            }
        } else {
            System.out.println("No customers returned");
        }
    }

    public static void printCustomers(ArrayList<Customer> customers) {
        if (customers.size() != 0) {
            for (Customer c : customers) {
                System.out.println("-------------------------------");
                System.out.println(c.getFirstName());
                System.out.println(c.getLastName());
                System.out.println(c.getPhone());
            }
        } else {
            System.out.println("No customers returned");
        }
    }

    public static void printCustomer(Customer c) {
        if (c != null) {
            System.out.println("-------------------------------");
            System.out.println(c.getFirstName());
            System.out.println(c.getLastName());
            System.out.println(c.getPhone());
        } else {
            System.out.println("No customer returned");
        }
    }


}
