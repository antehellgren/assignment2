# Access and expose a database 
 
## Intro

For this assignment the task was to build a Spring Boot Web API in java together with JDBC and Thymeleaf. Thymeleaf was used to make HTML pages that were run locally over the browser. The database used for this assignment were [Chinook](https://www.sqlitetutorial.net/sqlite-sample-database/). The assignment were done by utilizing pair programming. 
 
 
## Project Structure

The project includes three packages named controller, models and repository. The controller contains two classes (CustomerController and ThymeleafController). The customer-controller handles all the requests sent for the Spring boot web api and the thymeleaf-controller handles the view of the application to be displayed on the browser. The repositories classes (customerRepository and thymeleafRepository) contain all the functions to be able to communicate with the database. The model package contains classes that represent objects in the database.  
 
**Ante Hellgren och Alexander Idemark**

